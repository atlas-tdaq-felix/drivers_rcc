# IMPORTANT:
- This folder is maintained by Mark Donszelmann (Mark.Donszelmann@cern.ch)
- The driver API for FELIX is kept up to date with the Driver release via the procedure below:

## To Update the driver API in FELIX
- check for the latest driver on /eos/project/f/felix/www/user/dist/software/driver
- Update the script sync_with_rpm.sh to point to the latest release driver
- Login to machine with access to /eos/project, the commands rpm2cpio and cpio (lxplus for instance)
- run ./sync_with_rpm.sh
- check with git (git status, git diff) which files and what has changed.
- build it, using standard cmake
- commit to FELIX drivers_rcc project
- tag it with the proper tag 'drivers_rcc-<version>', where <version> is the same as the driver

## Remarks
- tdaq_for_felix is not used for this

# BELOW Notes from Markus

### NOTE:
  -- This is a copy of the driver sources distributed with the FELIX RPM maintained by Markus Joos (markus.joos@cern.ch)
  -- Once installed the RPM puts the sources in /usr/src/tdaq_sw_for_Flx-X.Y.Z
  -- In turn the RPM packages the sources of the drivers in https://svnweb.cern.ch/trac/atlastdaq/browser/DAQ/DataFlow

## build the drivers:

$ cd src
$ make

## load the drivers:

$ cd script
$ chmod 775 drivers_flx_local
$ sudo ./drivers_flx_local start

## autostart the drivers at boottime:

modify the drivers_flx_local script to make it point to /lib/modules/daq and copy it as drivers_flx to /etc/init.d.
In /etc/rc.d/rc.local it is specified that the script is to be run after booting of the machine is complete

## make the libraries available:

if you don't have access to AFS or you didn't install the drivers from RPM

$ export LD_LIBRARY_PATH=/path_to_the_svn_checkout/software/drivers_rcc/lib64

otherwise the libraries will be available at /usr/lib64 (or AFS)

## content of the drivers_rcc folder:

.
├── cmem_rcc
│   ├── cmem_rcc_common.h
│   ├── cmem_rcc_drv.h
│   └── cmem_rcc.h
├── DFDebug
│   ├── DFDebug.h
│   └── GlobalDebugSettings.h
├── drivers
├── flx
│   └── flx_common.h
├── io_rcc
│   ├── io_rcc_common.h
│   ├── io_rcc_driver.h
│   └── io_rcc.h
├── lib64
│   ├── libcmem_rcc.so
│   ├── libDFDebug.so
│   ├── libgetinput.so
│   ├── libio_rcc.so
│   └── librcc_error.so
├── rcc_error
│   └── rcc_error.h
├── README.txt
├── ROSGetInput
│   └── get_input.h
├── ROSRCDdrivers
│   └── tdaq_drivers.h
├── script
│   └── drivers_flx
│   └── drivers_flx_local
└── src
    ├── cmem_rcc.c
    ├── flx.c
    ├── io_rcc.c
    └── Makefile

For more detailed documentation please refer to:
  https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/FLX_support_SW.pdf
