#!/bin/sh -x
set -e

VERSION=4.13.0
SRCDIR=/afs/cern.ch/user/j/joos/public/Mark
DIR=/eos/project/f/felix/www/user/dist/software/driver
EXTRACT=rpm-extract
SRC=${EXTRACT}/usr/src/tdaq_sw_for_Flx-${VERSION}
INCLUDE=${EXTRACT}/usr/include
ETC=${EXTRACT}/etc

# cp ${SRCDIR}/tdaq_sw_for_Flx-${VERSION}-2dkms.noarch.rpm ${DIR}

mkdir -p ${EXTRACT}
cd ${EXTRACT}
rpm2cpio ${SRCDIR}/tdaq_sw_for_Flx-${VERSION}-2dkms.noarch.rpm | cpio -idmu --quiet
cd ..

cp ${INCLUDE}/cmem_rcc_common.h cmem_rcc
cp ${SRC}/cmem_rcc/cmem_rcc_drv.h cmem_rcc
cp ${INCLUDE}/cmem_rcc.h cmem_rcc

cp ${INCLUDE}/DFDebug.h DFDebug
cp ${INCLUDE}/GlobalDebugSettings.h DFDebug

cp ${INCLUDE}/flx_common.h flx

cp ${INCLUDE}/io_rcc_common.h io_rcc
cp ${SRC}/io_rcc/io_rcc_driver.h io_rcc
cp ${INCLUDE}/io_rcc.h io_rcc

# el7
ARCHOS=x86_64-centos7
LIB=${EXTRACT}/usr/lib64_cc7

rm -f lib64/${ARCHOS}/*
cp ${LIB}/libcmem_rcc.so lib64/${ARCHOS}/libcmem_rcc.so.${VERSION}	
ln -s libcmem_rcc.so.${VERSION} lib64/${ARCHOS}/libcmem_rcc.so
cp ${LIB}/libDFDebug.so lib64/${ARCHOS}/libDFDebug.so.${VERSION}
ln -s libDFDebug.so.${VERSION} lib64/${ARCHOS}/libDFDebug.so
cp ${LIB}/libgetinput.so lib64/${ARCHOS}/libgetinput.so.${VERSION}
ln -s libgetinput.so.${VERSION} lib64/${ARCHOS}/libgetinput.so
cp ${LIB}/libio_rcc.so lib64/${ARCHOS}/libio_rcc.so.${VERSION}
ln -s libio_rcc.so.${VERSION} lib64/${ARCHOS}/libio_rcc.so
cp ${LIB}/librcc_error.so lib64/${ARCHOS}/librcc_error.so.${VERSION}
ln -s librcc_error.so.${VERSION} lib64/${ARCHOS}/librcc_error.so

# el9
ARCHOS=x86_64-centos9
LIB=${EXTRACT}/usr/lib64_alma9

rm -f lib64/${ARCHOS}/*
cp ${LIB}/libcmem_rcc.so lib64/${ARCHOS}/libcmem_rcc.so.${VERSION}	
ln -s libcmem_rcc.so.${VERSION} lib64/${ARCHOS}/libcmem_rcc.so
cp ${LIB}/libDFDebug.so lib64/${ARCHOS}/libDFDebug.so.${VERSION}
ln -s libDFDebug.so.${VERSION} lib64/${ARCHOS}/libDFDebug.so
cp ${LIB}/libgetinput.so lib64/${ARCHOS}/libgetinput.so.${VERSION}
ln -s libgetinput.so.${VERSION} lib64/${ARCHOS}/libgetinput.so
cp ${LIB}/libio_rcc.so lib64/${ARCHOS}/libio_rcc.so.${VERSION}
ln -s libio_rcc.so.${VERSION} lib64/${ARCHOS}/libio_rcc.so
cp ${LIB}/librcc_error.so lib64/${ARCHOS}/librcc_error.so.${VERSION}
ln -s librcc_error.so.${VERSION} lib64/${ARCHOS}/librcc_error.so


cp ${INCLUDE}/rcc_error.h rcc_error

cp ${INCLUDE}/get_input.h ROSGetInput

cp ${SRC}/ROSRCDdrivers/*.h ROSRCDdrivers

cp ${ETC}/init.d/drivers_flx script

cp ${SRC}/src/* src
