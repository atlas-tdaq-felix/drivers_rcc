#!/bin/sh
#
# drivers_flx:       Starts TDAQ related drivers on a FELIX PC
#
# Version:      @(#) /etc/rc.d/init.d/drivers_flx 1.1
#
# chkconfig: 2345 95 5
# description: Starts and stops tdaq drivers at boot time and shutdown.
#
# hide: true

export CMEM_PARAMS="gfpbpa_size=8192 gfpbpa_quantum=4"
#GFP based cmem_rcc:

# See how we were called.
case "$1" in
  start)
        echo "Starting cmem driver "
        # load the module
        modprobe cmem_rcc $CMEM_PARAMS
        # remove old device nodes
        rm -f /dev/cmem_rcc
        rm -f /dev/cmem_rcc_2
        # get major number
        major=`awk "\\$2==\"cmem_rcc\" {print \\$1}" /proc/devices`
        echo major number for cmem_rcc is $major
        # make device node
        mknod /dev/cmem_rcc c $major 0
        mknod /dev/cmem_rcc_2 c $major 1
        #give permissions
        chmod 666 /dev/cmem_rcc
        chmod 666 /dev/cmem_rcc_2
        echo

        echo "Starting io_rcc driver "
        # load the module
        modprobe io_rcc
        # remove old device node
        rm -f /dev/io_rcc
        # get major number
        major=`awk "\\$2==\"io_rcc\" {print \\$1}" /proc/devices`
        echo major number for io_rcc is $major
        # make device node
        mknod /dev/io_rcc c $major 0
        # give permissions
        chmod 666 /dev/io_rcc
        echo
	
	export FELIXCARDS=`/sbin/lspci -n | grep -c -e "10ee:703" -e "10dc:042"`
	echo $FELIXCARDS flx PCIe endpoints found
	FIRSTCARD=0
	if [ $FELIXCARDS -gt 0 ]; then
	  echo "Starting flx driver "
	  # load the module
          modprobe flx
	  # remove old device nodes
	  rm -f /dev/flx*
	  # get major number
	  major=`awk "\\$2==\"flx\" {print \\$1}" /proc/devices`
	  echo major number for flx is $major
	  for (( c=$FIRSTCARD; c<$FELIXCARDS; c++ ))
	  do
	    echo "creating node /dev/flx$c"
	    # make device node
	    mknod /dev/flx$c c $major $c
	    # give permissions
	    chmod 666 /dev/flx$c
	  done
          for filename in /dev/xil_xvc/cfg_ioc*; do
              echo changing mode of $filename to 666
              chmod 666 $filename
          done
	  #create a link from flx0 to flx for compatibility between new driver and old library
	  ln -s /dev/flx0 /dev/flx
	  echo
	fi
        ;;

  stop)
        echo "Shutting down cmem_rcc driver "
        /sbin/rmmod cmem_rcc
        # remove old device nodes
        rm -f /dev/cmem_rcc
        rm -f /dev/cmem_rcc_2

        echo "Shutting down io_rcc driver "
        /sbin/rmmod io_rcc
        # remove old device node
        rm -f /dev/io_rcc

        /sbin/lsmod | grep flx
        if [ $? = 0 ]; then
          echo "Shutting down flx driver "
          /sbin/rmmod flx
	  # remove old device nodes
	  rm -f /dev/flx*
        fi
        ;;

   status)
        /sbin/lsmod | grep cmem_rcc
        if [ $? = 0 ]; then
          echo ""
          echo ">>>>>> Status of the cmem_rcc driver"
          echo ""
          more /proc/cmem_rcc
        fi

        /sbin/lsmod | grep io_rcc
        if [ $? = 0 ]; then
          echo ""
          echo ">>>>>> Status of the io_rcc driver"
          echo ""
          more /proc/io_rcc
        fi

        /sbin/lsmod | grep flx
        if [ $? = 0 ]; then
          echo ""
          echo ">>>>>> Status of the flx driver "
          echo ""
          more /proc/flx
        fi
        ;;

  *)
        echo "*** Usage: drivers_felix {start|stop|status}"
        exit 1

esac

exit 0
